# AllCraft Blog

* Uses Ghost
* Based on default theme for [Ghost](http://github.com/tryghost/ghost/).

## Backup Ghost

You can do a backup through the admin interface : [http://localhost:2368/ghost/settings/labs/](). Same page to restore the content. Check [https://www.ghostforbeginners.com/backup-ghost/]() for more information.


## Install

This easiest is to add a `docker-compose.yml` file at the root of the parent directory : 

```
version: "2"

services:
  ghost:
    image: ghost
    ports:
      - 2368:2368
    volumes:
      - ~/<you directory>/ghost:/var/lib/ghost
      - ~/<you directory>/ac-blog:/var/lib/ghost/themes/allcraft
```

Doing a `docker-compose up` will execute Ghost (and its database). Now you can go to [http://localhost:2368/ghost/settings/general/]() to set up the AllCraft Theme and [http://localhost:2368/ghost]() to create new blogs for example.

## Check Amazon AWS

The blog is hosted on Amazon AWS. Once you [log in](https://allcraft.signin.aws.amazon.com/console) you can see all the [instances](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:sort=instanceId). The blog is hosted on instance `i-0f59e5ed204f64a39` and the public DNS `ec2-35-156-173-143.eu-central-1.compute.amazonaws.com`. Going to this [URL](ec2-35-156-173-143.eu-central-1.compute.amazonaws.com) you should see the blog.  

You can SSH the machine by going : 

``` 
ssh -i ~/Google\ Drive/ALLCRAFT/Keys/ac_ghost.pem ubuntu@ec2-35-156-173-143.eu-central-1.compute.amazonaws.com
```


## Deploying a new version of the Web Site


### Push the Docker image our AllCraft Docker HUB


### Pull the Docker image on AWS

Log into the AWS instance and do the following : 

```
docker-compose pull
docker-compose up -d
```

## Upgrading Docker compose

If needed, you can update the version of Docker Compose on the AWS instance

```
sudo -i curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo -i chmod +x /usr/local/bin/docker-compose
```
